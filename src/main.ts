import { createApp } from 'vue'

import MDS from '@michael_v92/test_vue_3_package';

import App from './App.vue'

import '@michael_v92/test_vue_3_package/dist/test_vue_3_package.css';

createApp(App)
    .use(MDS)
    .mount('#app')
